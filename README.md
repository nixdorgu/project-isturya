# Isturya

A Flutter messaging application.

## Getting Started

This project is a simple messaging app built with *Flutter* and *Firebase*.

Dependencies include the following:

* firebase_analytics: ^5.0.11
* firebase_auth: ^0.15.4
* firebase_core: 0.4.4
* cloud_firestore: ^0.13.4
* provider: ^4.0.4
* uuid: ^2.0.4
* intl: ^0.16.1

### Notes:

This project is still due for updates particularly in terms of:
* Refactoring
* Completeness
* Functionality
* Efficiency
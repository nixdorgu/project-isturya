// flutter or dart packages
import 'package:flutter/material.dart';

// app packages
import 'package:isturya/models/message_model.dart';
import 'package:isturya/models/user_model.dart';
import 'package:isturya/models/chatroom.dart';
import 'package:isturya/providers/user_data.dart';

// dependency packages
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class DatabaseService {

  // user operations
  static Future<DocumentSnapshot> getUserDocFromId(
      {String currentUserId}) async {
    final Future<DocumentSnapshot> userDocument =
        Firestore.instance.collection('users').document(currentUserId).get();
    return userDocument;
  }

//  todo: update and add profile page
  // update: todo copy this to another project and delte
  // isturya is messenger-like - i.e. you can't change your pp/name within the app
  static void updateUserData(User user) {
    Firestore.instance.collection('users').document(user.id).updateData({
      'name': user.name,
      'profileURL': user.profileURL,
    });
  }

  // user searches for other users

  // PS: update searching logic
  // todo: add metadata to improve searching!!!
  // todo: update searching logic, urgently
  static Future<QuerySnapshot> searchForUsers(String name) async {
    Future<QuerySnapshot> users = Firestore.instance
        .collection('users')
        .where('name', isGreaterThan: name)
        .getDocuments();
    return users;
  }

  static Stream<QuerySnapshot> getFrequentlyContacted(String currentUserId) {
    // no inequality operator in firestore 
    Stream<QuerySnapshot> users = Firestore.instance
        .collection('messages')
        .where('show', arrayContains: currentUserId)
        .where('messages', isGreaterThan: [])
        .limit(5)
        .orderBy('messages', descending: true)
        .snapshots();

    return users;
  }


// chatroom operations

  static createChatroom({String currentUserId, String otherUserId}) {
    String chatroomId = Uuid().v4();
    bool self = currentUserId == otherUserId;

    Firestore.instance.collection('messages').document(chatroomId).setData({
      'users': [currentUserId, otherUserId],
      'show': [currentUserId, otherUserId], // rethink this.
      'messages': [],
    });

    Firestore.instance.collection('users').document(currentUserId).updateData({'chatrooms.$otherUserId': chatroomId});
    if (!self) Firestore.instance.collection('users').document(otherUserId).updateData({'chatrooms.$currentUserId': chatroomId});

    return chatroomId;
  }

  static void sendMessageToChatroom({BuildContext context, Chatroom chatroom, Message message,
  }) async {
    Firestore.instance.collection('messages').document(chatroom.id).updateData({
      'messages': FieldValue.arrayUnion([message.toMap()])
    });


    // rethink this.
    Firestore.instance.collection('messages').document(chatroom.id).updateData({
      'show': FieldValue.arrayUnion([Provider.of<UserData>(context, listen: false).userId])
    });

    Firestore.instance
        .collection('messages')
        .document(chatroom.id)
        .updateData({'timestamp': message.timestamp});
  }


  // pseudo-delete. message still in database but appears deleted to current user
  // flaw: user who delete doesn't receive new messages due to chatroom duplication
  // todo: remedy bug by updating search screen or by revisiting db structure && chatroom creation code above

  static deleteChatroom({String chatroomId, String currentUserId, String otherUserId}) {
    try {
      if (currentUserId == otherUserId) {
        Firestore.instance.collection('users').document(otherUserId).updateData(
            {'chatrooms.$currentUserId': FieldValue.delete()});
        Firestore.instance.collection('users').document(currentUserId).updateData(
            {'chatrooms.$otherUserId': FieldValue.delete()});
        Firestore.instance.collection('messages').document(chatroomId).delete();
      }  else {
        Firestore.instance.collection('messages').document(chatroomId).updateData({'show': FieldValue.arrayRemove([currentUserId])});
      }

    } catch(error) {
      print(error);
    }
  }


  static Future<List<Chatroom>> getChatroomStream({String currentUserId}) async {
    QuerySnapshot chatroom = await Firestore.instance
        .collection('messages')
        .where('show', arrayContains: currentUserId)
        .orderBy('timestamp', descending: true)
        .getDocuments();

    return chatroom.documents.map((doc) => Chatroom.fromDoc(doc)).toList();
  }

  static Future<QuerySnapshot> getChatroomForSearch({String currentUserId, String otherUserId}) {
    Future<QuerySnapshot> chatroom = Firestore.instance
        .collection('messages')
        .where('users', isEqualTo: [currentUserId, otherUserId]).getDocuments();
    return chatroom;
  }
}

// flutter or dart packages
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';

// app packages
import 'package:isturya/screens/home.dart';
import 'package:isturya/dialogs/on_platform_exception_dialog.dart';
import 'package:isturya/providers/chatroom_model.dart';
import 'package:isturya/providers/user_data.dart';
import 'package:isturya/screens/authentication/login.dart';

// dependency packages
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider/provider.dart';

class AuthenticationService {
  static void signup(
      BuildContext context, String name, String email, String password) async {
    try {
      AuthResult authResult =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      FirebaseUser user = authResult.user;

      if (user != null) {
        Firestore.instance.collection('users').document(user.uid).setData({
          'id': user.uid,
          'name': name,
          'email': email,
          'profileURL': '',
          'chatrooms': {},
        });

        Provider.of<UserData>(context, listen: false).userId = user.uid;
        Provider.of<ChatroomModel>(context, listen: false).currentUserId =
            user.uid;
      }

      Navigator.pushNamed(context, HomeScreen.id);
    } on PlatformException catch (error) {
      String errorCode;

      switch (error.code) {
        case 'ERROR_WEAK_PASSWORD':
          errorCode = 'Weak Password.';
          break;
        case 'ERROR_INVALID_EMAIL':
          errorCode = 'Invalid Email.';
          break;
        case 'ERROR_EMAIL_ALREADY_IN_USE':
          errorCode = 'Email Already In Use';
          break;
      }

      return platformExceptionDialog(
        context: context,
        errorTitle: errorCode,
        errorBody: error.message,
      );
    }
  }

  static void signin(
      BuildContext context, String email, String password) async {
    try {
      AuthResult authResult =
          await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

      Navigator.pushReplacementNamed(context, HomeScreen.id);

    } on PlatformException catch (error) {
      String errorCode;

      switch (error.code) {
        case 'ERROR_WRONG_PASSWORD':
          errorCode = 'Incorrect Password.';
          break;
        case 'ERROR_INVALID_EMAIL':
          errorCode = 'Invalid Email.';
          break;
        case 'ERROR_USER_NOT_FOUND':
          errorCode = 'Error 404';
          break;
      }

      return platformExceptionDialog(
        context: context,
        errorTitle: errorCode,
        errorBody: error.message,
      );
    }
  }

  static void logout(BuildContext context) {
    FirebaseAuth.instance.signOut();

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (_) => LoginScreen(),
      ),
    );
  }
}

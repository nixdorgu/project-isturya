import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

platformExceptionDialog(
    {BuildContext context, String errorTitle, String errorBody}) {
  errorTitle = errorTitle ?? 'UH OH!';

  return showDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return Platform.isIOS
          ? iOS(
              context: context,
              errorTitle: errorTitle,
              errorBody: errorBody,
            )
          : android(
              context: context,
              errorTitle: errorTitle,
              errorBody: errorBody,
            );
    },
  );
}

Widget iOS({BuildContext context, String errorTitle, String errorBody}) {
  return CupertinoAlertDialog(
    title: Text(errorTitle),
    content: Text(errorBody),
  );
}

Widget android({BuildContext context, String errorTitle, String errorBody}) {
  return AlertDialog(
    title: Text(errorTitle),
    content: Text(errorBody),
  );
}

import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:isturya/models/user_model.dart';

searchScreenDialog({User user, BuildContext context, Function pushToChatroom}) {
  bool hasPP = user.profileURL != null && user.profileURL.isNotEmpty;

  return Platform.isIOS
      ? showCupertinoDialog(
          context: context,
          useRootNavigator: true,
          builder: (_) => iOS(context, user, hasPP, pushToChatroom),
        )
      : showDialog(
          context: context,
          barrierDismissible: true,
          builder: (_) => android(context, user, hasPP, pushToChatroom),
        );
}

Widget iOS(BuildContext context, User user, bool hasPP, Function pushToChatroom) {
  return CupertinoAlertDialog(
    title: Center(
      child: Row(
        children: <Widget>[
          CircleAvatar(
            backgroundImage: hasPP
                ? NetworkImage(user.profileURL)
                : AssetImage('assets/images/user_placeholder.png'),
            radius: 30,
          ),
          Padding(
            padding: EdgeInsets.only(left: 15),
            child: Text(user.name),
          ),
        ],
      ),
    ),
    actions: <Widget>[
      FlatButton(
        child: Text('Message ${user.name.split(' ')[0]}'),
        onPressed: () {
          Navigator.pop(context);
          pushToChatroom();
        },
      ),
    ],
  );
}

Widget android(BuildContext context, User user, bool hasPP, Function pushToChatroom) {
  return AlertDialog(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(20),
    ),
    title: Center(
      child: Row(
        children: <Widget>[
          CircleAvatar(
            backgroundImage: hasPP
                ? NetworkImage(user.profileURL)
                : AssetImage('assets/images/user_placeholder.png'),
            radius: 30,
          ),
          Padding(
            padding: EdgeInsets.only(left: 15.0),
            child: Text(user.name),
          ),
        ],
      ),
    ),
    actions: <Widget>[
      FlatButton(
        child: Text('Message ${user.name.split(' ')[0]}'),
        onPressed: () {
          Navigator.pop(context);
          pushToChatroom();
        },
      )
    ],
  );
}

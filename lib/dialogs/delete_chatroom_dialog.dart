import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:isturya/models/chatroom.dart';
import 'package:isturya/providers/chatroom_model.dart';

import 'package:provider/provider.dart';


const options = ['Delete Chatroom?', 'Delete', 'Cancel'];

deleteChatroomDialog({BuildContext context, Chatroom chatroom}) {
  return Platform.isIOS ? showCupertinoDialog(
      context: context,
      useRootNavigator: true,
      builder: (_) => iOS(context, chatroom),
  ) : showDialog(
    context: context,
    barrierDismissible: true,
    builder: (_) => android(context, chatroom),
  );
}

Widget iOS(BuildContext context, Chatroom chatroom) {
  return CupertinoAlertDialog(
    title: Text(options[0]),
    actions: <Widget>[
      CupertinoActionSheetAction(
        isDefaultAction: true,
        child: Text(
          options[1],
          style: TextStyle(
            color: Colors.red,
          ),
        ),
        onPressed: () {
          Provider.of<ChatroomModel>(context, listen: false)
              .deleteChatroom(chatroom);
          Navigator.of(context, rootNavigator: true).pop();
        },
      ),
      CupertinoActionSheetAction(
        child: Text(options[2]),
        onPressed: () {
          Navigator.of(context, rootNavigator: true).pop();
        },
      ),
    ],
  );
}

Widget android(BuildContext context, Chatroom chatroom) {
  return AlertDialog(
    title: Text(options[0]),
    actions: <Widget>[
      FlatButton(
        child: Text(
          options[1],
          style: TextStyle(
            fontSize: 20,
            color: Colors.red[700],
          ),
        ),
        onPressed: () {
          Provider.of<ChatroomModel>(context, listen: false)
              .deleteChatroom(chatroom);
          Navigator.of(context, rootNavigator: true).pop();
        },
      ),
      FlatButton(
        child: Text(
          options[2],
          style: TextStyle(
            fontSize: 20,
          ),
        ),
        onPressed: () {
          Navigator.of(context, rootNavigator: true).pop();
        },
      ),
    ],
  );
}

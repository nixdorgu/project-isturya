import 'package:flutter/material.dart';

import 'package:isturya/providers/chatroom_model.dart' as model;
import 'package:isturya/screens/home.dart';
import 'package:isturya/screens/authentication/login.dart';
import 'package:isturya/providers/user_data.dart';

import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SplashScreen extends StatefulWidget {
  static final String id = 'splash_screen';
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with TickerProviderStateMixin {
  Animation<double> _animation;
  AnimationController _animationController;

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      duration: Duration(milliseconds: 1500),
      vsync: this,
      value: 0,
    );

    _animation = CurvedAnimation(
      curve: Curves.bounceInOut,
      parent: _animationController,
    );

    _animationController.forward();

    Future.delayed(
      Duration(milliseconds: 2500),
      () => Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) => _getScreenId(),
        ),
      ),
    );
  }

  Widget _getScreenId() {
    return StreamBuilder<FirebaseUser>(
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasData) {
          Provider.of<UserData>(context).userId = snapshot.data.uid;
          Provider.of<model.ChatroomModel>(context).currentUserId = snapshot.data.uid;

          return HomeScreen();
        }

          return LoginScreen();
      },
      stream: FirebaseAuth.instance.onAuthStateChanged,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFF5F5F5),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width,
      child: ScaleTransition(
        scale: _animation,
        alignment: Alignment.center,
        child: Center(
          child: Center(
            child: RichText(
              textDirection: TextDirection.ltr,
              text: TextSpan(
                text: 'isturya',
                style: TextStyle(
//                  fontFamily: 'Dosis',
                  fontSize: 60.0,
                  color: Color(0xFF1E1E1E),
                ),
                children: <InlineSpan>[
                  TextSpan(
                    text: '.',
                    style: TextStyle(
                      fontFamily: 'Dosis',
                      fontSize: 60.0,
                      color: Colors.blueAccent[700],
                      letterSpacing: 1.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

//todo: improve ui -> add assets/fonts/
//todo: swap splash for legitimate launch screen && change appicon when marco emails logo
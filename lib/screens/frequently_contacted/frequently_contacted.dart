import 'package:flutter/material.dart';

import 'package:isturya/models/chatroom.dart';
import 'package:isturya/services/Database.dart';
import 'package:isturya/models/user_model.dart';
import 'package:isturya/screens/messages/chat_screen_item.dart';

import 'package:cloud_firestore/cloud_firestore.dart';

class FrequentlyContacted extends StatefulWidget {
  final String currentUserId;

  FrequentlyContacted({this.currentUserId});

  @override
  _FrequentlyContactedState createState() => _FrequentlyContactedState();
}

class _FrequentlyContactedState extends State<FrequentlyContacted> {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15),
            child: Text(
              'Frequently Contacted',
              style: TextStyle(
                color: Colors.blueGrey,
                fontSize: 18,
                fontWeight: FontWeight.bold,
                letterSpacing: 1.0,
              ),
            ),
          ),
          Container(
            height: 120,
            child: StreamBuilder(
              stream: DatabaseService.getFrequentlyContacted(widget.currentUserId),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }

                // todo: consider if the state changes enough to use a provider model
                // todo: for new users, shows CPI constantly, change to provider to eradicate

                if (snapshot.data.documents.length == 0) {
                  return Center(
                    child: Text('No frequently contacted users yet.'),
                  );
                }

                return ListView.builder(
                  itemCount: snapshot.data.documents.length,
                  padding: EdgeInsets.only(left: 10),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    DocumentSnapshot chatroomDocument =
                    snapshot.data.documents[index];

                    Chatroom chatroom = Chatroom.fromDoc(chatroomDocument);

                    // self-chat logic -> figure out how to do so efficiently as ! doesn't exist in Firestore
                    final isMe = chatroom.users
                        .where((String id) => id != widget.currentUserId)
                        .toList().isEmpty;

                    if (isMe) return SizedBox.shrink();

                    String otherUserId = chatroom.users
                        .where((String id) => id != widget.currentUserId)
                        .toList().first;

                    return FutureBuilder(
                      future: DatabaseService.getUserDocFromId(
                          currentUserId: otherUserId),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (!snapshot.hasData) {
                          return CircleAvatar(
                            backgroundColor: Colors.transparent,
                            radius: 35,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
                        }

                        if (snapshot.hasData && snapshot.data == null) {
                          return Container(
                            width: MediaQuery.of(context).size.width * 0.75,
                            child: Center(
                              child: Text('No frequently contacted users yet.'),
                            ),
                          );
                        }

                        User user = User.fromDoc(snapshot.data);
                        bool hasPP = user.profileURL != null && user.profileURL.isNotEmpty;

                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) =>
                                    ChatScreen(chatroom: chatroom, user: user),
                              ),
                            );
                          },
                          child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Column(
                              children: [
                                CircleAvatar(
                                  backgroundImage: hasPP ? NetworkImage(user.profileURL) : AssetImage('assets/images/user_placeholder.png'),
                                  radius: 30,
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Text(
                                  user.name.split(' ')[0],
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16,
                                      color: Colors.blueGrey),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

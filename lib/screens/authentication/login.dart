import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';

import 'package:isturya/screens/authentication/signup.dart';
import 'package:isturya/services/authentication.dart';

class LoginScreen extends StatefulWidget {
  static final String id = 'login_screen';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  String _email, _password;

  _submit(context) async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      AuthenticationService.signin(context, _email, _password);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            RichText(
              text: TextSpan(
                text: 'Isturya',
                style: TextStyle(
//                    fontFamily: 'Dosis',
                    fontSize: 60.0,
                    color: Color(0xFF1E1E1E),
                    letterSpacing: 1.0),
                children: <InlineSpan>[
                  TextSpan(
                    text: '.',
                    style: TextStyle(
//                      fontFamily: 'Dosis',
                      fontSize: 60.0,
                      color: Theme.of(context).primaryColor,
                      letterSpacing: 1.0,
                    ),
                  ),
                ],
              ),
            ),
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 60.0, vertical: 5.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Email',
                      ),
                      validator: (input) => !input.contains('@')
                          ? 'Please enter a valid email address'
                          : null,
                      onSaved: (input) => _email = input,
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 60.0, vertical: 5.0),
                    child: TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        labelText: 'Password',
                      ),
                      validator: (input) => input.length < 6
                          ? 'Password must be between 6 to 12 characters'
                          : null,
                      onSaved: (input) => _password = input,
                    ),
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 60.0, vertical: 5.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: RaisedButton(
                        child: Text(
                          'Login',
                          style: TextStyle(
//                            fontFamily: 'Dosis',
                            color: Color(0xFF1E1E1E),
                          ),
                        ),
                        onPressed: () {
                          _submit(context);
                        },
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Container(
                    child: RichText(
                      text: TextSpan(
                        text: "Don't have an account yet? ",
                        style: TextStyle(
                          color: Color(0xFF1E1E1E),
//                          fontFamily: 'Dosis',
                          fontSize: 16,
                        ),
                        children: [
                          TextSpan(
                            text: 'Sign Up',
                            style: TextStyle(
                              color: Color(0xFF1E1E1E),
//                              fontFamily: 'Dosis',
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.underline,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () => Navigator.of(context)
                                  .pushNamed(SignupScreen.id),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

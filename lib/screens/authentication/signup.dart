import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:isturya/services/authentication.dart';

class SignupScreen extends StatefulWidget {
  static final String id = 'signup_screen';
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final _formKey = GlobalKey<FormState>();
  String _name, _email, _password;

  _submit(context) async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      AuthenticationService.signup(context, _name, _email, _password);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            RichText(
              text: TextSpan(
                text: 'Isturya',
                style: TextStyle(
//                  fontFamily: 'Dosis',
                  fontSize: 60.0,
                  color: Color(0xFF1E1E1E),
                  letterSpacing: 1.0,
                ),
                children: <InlineSpan>[
                  TextSpan(
                    text: '.',
                    style: TextStyle(
//                      fontFamily: 'Dosis',
                      fontSize: 60.0,
                      color: Theme.of(context).primaryColor,
                      letterSpacing: 1.0,
                    ),
                  ),
                ],
              ),
            ),
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding:
                    EdgeInsets.symmetric(horizontal: 60.0, vertical: 5.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Name',
                      ),
                      validator: (input) => input.trim().isEmpty
                          ? 'Name field must be empty'
                          : null,
                      onSaved: (input) => _name = input,
                    ),
                  ),
                  Padding(
                    padding:
                    EdgeInsets.symmetric(horizontal: 60.0, vertical: 5.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Email',
                      ),
                      validator: (input) => !input.contains('@')
                          ? 'Please enter a valid email address'
                          : null,
                      onSaved: (input) => _email = input,
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding:
                    EdgeInsets.symmetric(horizontal: 60.0, vertical: 5.0),
                    child: TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        labelText: 'Password',
                      ),
                      validator: (input) => input.length < 6
                          ? 'Password must be between 6 to 12 characters'
                          : null,
                      onSaved: (input) => _password = input,
                    ),
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding:
                    EdgeInsets.symmetric(horizontal: 60.0, vertical: 5.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: RaisedButton(
                        child: Text(
                          'Sign Up',
                          style: TextStyle(
//                            fontFamily: 'Dosis',
                            color: Color(0xFF1E1E1E),
                          ),
                        ),
                        onPressed: () {
                          _submit(context);
                        },
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Container(
                    child: RichText(
                      text: TextSpan(
                        text: "Already have an account? ",
                        style: TextStyle(
                          color: Color(0xFF1E1E1E),
                          fontFamily: 'Dosis',
                          fontSize: 16,
                        ),
                        children: [
                          TextSpan(
                            text: 'Login',
                            style: TextStyle(
                              color: Color(0xFF1E1E1E),
//                              fontFamily: 'Dosis',
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.underline,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () => Navigator.pop(context),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

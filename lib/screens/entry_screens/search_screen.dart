import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:isturya/dialogs/search_screen_dialog.dart';
import 'package:isturya/models/chatroom.dart';
import 'package:isturya/models/user_model.dart';
import 'package:isturya/screens/messages/chat_screen_item.dart';
import 'package:isturya/services/database.dart';

import 'package:cloud_firestore/cloud_firestore.dart';

class SearchScreen extends StatefulWidget {
  final currentUserId;

  SearchScreen({this.currentUserId});

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  TextEditingController _searchController = TextEditingController();
  Future<QuerySnapshot> _users;

  pushToChatroom({User user}) async {
    Chatroom chatroom;

    await DatabaseService.getChatroomForSearch(
      currentUserId: widget.currentUserId,
      otherUserId: user.id,
    ).then((data) {
      // no exisitng chatroom to send messages to
      // update logic to accomodate deleteChatroom logic change

      if (data.documents.isEmpty) {
        var chatroomId = DatabaseService.createChatroom(
          currentUserId: widget.currentUserId,
          otherUserId: user.id,
        );

        chatroom = Chatroom(
          id: chatroomId,
          users: [widget.currentUserId, user.id],
          messages: [],
        );
      } else {
        // pre-exisiting chatroom
        chatroom = Chatroom.fromDoc(data.documents[0]);
      }
    });

    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => ChatScreen(
          user: user,
          chatroom: chatroom,
        ),
      ),
    );

    await _clearSearch();
  }

  _buildUserTile(User user, BuildContext context) {
    bool hasPP = user.profileURL != null && user.profileURL.isNotEmpty;

    return ListTile(
      contentPadding: EdgeInsets.only(left: 20, top: 4),
      leading: CircleAvatar(
        radius: 20,
        backgroundImage: hasPP
            ? NetworkImage(user.profileURL)
            : AssetImage('assets/images/user_placeholder.png'),
      ),
      title: Text(user.name),
      onTap: () {
        // test this function
        searchScreenDialog(
          context: context,
          user: user,
          pushToChatroom: pushToChatroom(user: user),
        );
      },
    );
  }

  // update logic and ui
  _clearSearch() {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) => _searchController.clear(),
    );

    setState(() {
      _users = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        elevation: 0.0,
        title: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.white60,
            ),
            borderRadius: BorderRadius.all(Radius.circular(40)),
          ),
          child: TextField(
            cursorColor: Colors.white60,
            controller: _searchController,
            decoration: InputDecoration(
              fillColor: Colors.white54,
              contentPadding: EdgeInsets.symmetric(vertical: 15.0),
              border: InputBorder.none,
              hintText: 'Search',
              hintStyle: TextStyle(
                color: Colors.white54,
              ),
              prefixIcon: Icon(
                Icons.search,
                size: 30.0,
                color: Colors.white60,
              ),
              suffixIcon: IconButton(
                icon: Icon(
                  Icons.clear,
                  color: Colors.white60,
                ),
                onPressed: _clearSearch,
              ),
            ),
            onSubmitted: (input) {
              if (input.isNotEmpty) {
                setState(() {
                  _users = DatabaseService.searchForUsers(input);
                });
              }
            },
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 10),
        child: Column(
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xFFFEF9EB),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                  child: _users == null
                      ? Center(
                          child: Text(
                            'Search for users.',
                            style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey[300],
                            ),
                          ),
                        )
                      : FutureBuilder(
                          future: _users,
                          builder: (context, snapshot) {
                            if (!snapshot.hasData) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            }

                            if (snapshot.data.documents.length == 0) {
                              return Center(
                                child: Text(
                                  'No users found.',
                                  style: TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blueGrey[300],
                                  ),
                                ),
                              );
                            }

                            return ListView.builder(
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (BuildContext context, int index) {
                                User user = User.fromDoc(
                                  snapshot.data.documents[index],
                                );
                                return _buildUserTile(user, context);
                              },
                            );
                          },
                        ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// see todo
// currently ' ' to see all users

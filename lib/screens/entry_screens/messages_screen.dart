import 'package:flutter/material.dart';
import 'package:isturya/screens/messages/messages.dart';

import 'package:isturya/screens/frequently_contacted/frequently_contacted.dart';
import 'package:isturya/providers/user_data.dart';

import 'package:provider/provider.dart';

class MessagesScreen extends StatefulWidget {
  static String id = 'messages_screen';

  @override
  _MessagesScreenState createState() => _MessagesScreenState();
}

class _MessagesScreenState extends State<MessagesScreen> {
  @override
  Widget build(BuildContext context) {
    final String currentUserId = Provider.of<UserData>(context).userId;

    FocusScope.of(context).unfocus();
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xFFFEF9EB), // Theme.of(context).accentColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
                child: Column(
                  children: <Widget>[
                    FrequentlyContacted(
                      currentUserId: currentUserId,
                    ),
                    Messages(),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

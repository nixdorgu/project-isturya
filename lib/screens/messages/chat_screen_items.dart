import 'package:flutter/material.dart';

import 'package:isturya/models/message_model.dart';

import 'package:intl/intl.dart';

class ChatListItem extends StatelessWidget {
  final bool isMe, isSameDay;
  final Message message;

  ChatListItem({this.isMe, this.isSameDay, this.message});

  @override
  Widget build(BuildContext context) {

    return Container(
      width: MediaQuery.of(context).size.width * .75,
      margin: isMe
          ? EdgeInsets.only(
        top: 8,
        bottom: 8,
        left: 80,
      )
          : EdgeInsets.only(
        top: 8,
        bottom: 8,
        right: 80,
      ),
      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
      decoration: BoxDecoration(
        color: isMe ? Theme.of(context).primaryColor : Color(0xFFFFEFEE),
        borderRadius: isMe
            ? BorderRadius.only(
          topLeft: Radius.circular(20),
          bottomLeft: Radius.circular(20),
        )
            : BorderRadius.only(
          topRight: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 5),
          Text(
            message.content,
            style: TextStyle(
              color: isMe ? Colors.white54 : Colors.blueGrey[300],
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: 2),
          isSameDay
              ? Text(
            '${DateFormat('hh:mm aaa').format(message.timestamp.toDate())}',
            style: TextStyle(
              color: isMe ? Colors.white30 : Colors.blueGrey[100],
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          )
              : Text(
            '${DateFormat('hh:mm aaa on M/d/yyyy').format(message.timestamp.toDate())}',
            style: TextStyle(
              color: isMe ? Colors.white30 : Colors.blueGrey[100],
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}

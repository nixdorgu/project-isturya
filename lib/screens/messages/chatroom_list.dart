import 'package:flutter/material.dart';

import 'package:isturya/models/chatroom.dart';
import 'package:isturya/providers/chatroom_model.dart' as model;
import 'package:isturya/screens/messages/chatroom_list_item.dart';

import 'package:provider/provider.dart';

class ChatroomList extends StatelessWidget {
  final List<Chatroom> chatroom;

  ChatroomList({@required this.chatroom});

  @override
  Widget build(BuildContext context) {
    bool loading =
        Provider.of<model.ChatroomModel>(context).state == model.State.loading;
//    checkConnection(); //todo: implement for connectivity check

    if (loading) {
      return Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.lightBlue,
        ),
      );
    }

    return chatroom.isEmpty
        ? Container(
            child: Center(
              child: Text(
                'No messages yet.',
                style: TextStyle(fontSize: 20, color: Colors.blueGrey[300]),
              ),
            ),
          )
        : Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Color(0xFFFEF9EB),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
                child: ListView(
                  children: getChildrenTasks(),
                ),
              ),
            ),
          );
  }

  List<Widget> getChildrenTasks() {
    return chatroom
        .map((chatroom) => ChatroomListItem(chatroom: chatroom))
        .toList();
  }

//  checkConnection() async {
//    final result = await InternetAddress.lookup('google.com');
//    connectivity package -> find stable internet to dl package
//    print(result);
//  }
}

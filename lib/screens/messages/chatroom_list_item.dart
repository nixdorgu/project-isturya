import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:isturya/services/Database.dart';
import 'package:isturya/providers/user_data.dart';
import 'package:isturya/models/user_model.dart';
import 'package:isturya/models/chatroom.dart';
import 'package:isturya/screens/messages/chat_screen_item.dart';
import 'package:isturya/dialogs/delete_chatroom_dialog.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';

class ChatroomListItem extends StatelessWidget {
  final Chatroom chatroom;

  ChatroomListItem({this.chatroom});

  @override
  Widget build(BuildContext context) {
    final currentUserId = Provider.of<UserData>(context).userId;
    final lastMessage = chatroom.messages.last;

    initializeDateFormatting('en_us', null);

//    retrieve user data
    String otherUserId = lastMessage.sender == currentUserId
        ? lastMessage.recipient
        : lastMessage.sender;

    return FutureBuilder(
      future: DatabaseService.getUserDocFromId(currentUserId: otherUserId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }

//        obtain user data
        User user = User.fromDoc(snapshot.data);
        bool hasPP = user.profileURL != null && user.profileURL.isNotEmpty;

//        obtain timestamps
        Timestamp now = Timestamp.now();
        Timestamp then = lastMessage.timestamp;

//        compare timestamps for date/time formatting
        Duration difference = (now.toDate()).difference(then.toDate());
        bool isSameDay = difference.inHours <= 23;

        return ListTile(
            contentPadding: EdgeInsets.only(bottom: 10, right: 15, left: 20),
            leading: CircleAvatar(
              backgroundImage: hasPP
                  ? NetworkImage(user.profileURL)
                  : AssetImage('assets/images/user_placeholder.png'),
              radius: 30,
            ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * .5,
                      child: Text(
                        user.name,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                      padding: EdgeInsets.only(bottom: 3.0),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * .55,
                      child: Text(
                        lastMessage.content,
                        style: TextStyle(
                          color: lastMessage.sender == currentUserId
                              ? Colors.blueGrey[500]
                              : Colors.blueGrey[700],
                          fontWeight: lastMessage.sender == currentUserId
                              ? FontWeight.w500
                              : FontWeight.w700,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: lastMessage.sender != currentUserId
                      ? [
                          Icon(
                            Icons.notifications,
                            color: Colors.grey,
                          )
                        ]
                      : isSameDay
                          ? [
                              Text(
                                '${DateFormat('h:mm aaa').format(
                                  lastMessage.timestamp.toDate(),
                                )}',
                                style: TextStyle(
                                  color: Colors.blueGrey[300],
                                ),
                              ),
                              Text(''),
                            ]
                          : [
                              Text(
                                '${DateFormat('h:mm aaa').format(
                                  lastMessage.timestamp.toDate(),
                                )}',
                                style: TextStyle(
                                  color: Colors.blueGrey[300],
                                ),
                              ),
                              SizedBox(height: 1.5),
                              Text(
                                '${DateFormat('M/d/yyyy').format(
                                  lastMessage.timestamp.toDate(),
                                )}',
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.blueGrey[100],
                                ),
                              ),
                            ],
                ),
              ],
            ),
            onTap: () async {
              await DatabaseService.getUserDocFromId(currentUserId: otherUserId)
                  .then((value) {
                User otherUser = User.fromDoc(value);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) =>
                        ChatScreen(chatroom: chatroom, user: otherUser),
                  ),
                );
              });
            },
            onLongPress: () {
              return deleteChatroomDialog(context: context, chatroom: chatroom);
            });
      },
    );
  }
}

import 'package:flutter/material.dart';

import 'package:isturya/providers/chatroom_model.dart';
import 'package:isturya/screens/messages/chatroom_list.dart';

import 'package:provider/provider.dart';

class Messages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Consumer<ChatroomModel>(
        builder: (context, chatrooms, child) => ChatroomList(
          chatroom: chatrooms.allChatrooms,
        ),
      ),
    );
  }
}
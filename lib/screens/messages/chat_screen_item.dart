import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:isturya/models/chatroom.dart';
import 'package:isturya/models/message_model.dart';
import 'package:isturya/providers/user_data.dart';
import 'package:isturya/models/user_model.dart';
import 'package:isturya/providers/chatroom_model.dart' as model;
import 'package:isturya/screens/messages/chat_screen_items.dart';

import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ChatScreen extends StatefulWidget {
  Chatroom chatroom;
  User user;

  ChatScreen({this.chatroom, this.user});

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final ScrollController _scrollController = ScrollController();
  final TextEditingController _controller = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
//    glitchy
    Future.delayed(
      Duration(microseconds: 0),
      () => _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: Duration(microseconds: 1),
          curve: Curves.ease),
    );

    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _controller.dispose();
    super.dispose();
  }

  Widget buildMessageComposer(context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: Theme.of(context).primaryColor),
        ),
      ),
      padding: EdgeInsets.symmetric(horizontal: 10),
      height: 70,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: TextField(
              autofocus: false,
              controller: _controller,
              decoration: InputDecoration.collapsed(
                hintText: 'Send a message',
              ),
              textCapitalization: TextCapitalization.sentences,
            ),
          ),
          IconButton(
              icon: Icon(Icons.send),
              iconSize: 25,
              color: Theme.of(context).primaryColor,
              onPressed: () async {
                CupertinoAlertDialog(
                  actions: <Widget>[
                    CircularProgressIndicator(),
                  ],
                );

                if (_controller.text.trim().isNotEmpty) {
                  Message message = Message(
                    sender:
                        Provider.of<UserData>(context, listen: false).userId,
                    recipient: widget.user.id,
                    content: _controller.text,
                    timestamp: Timestamp.now(),
                  );

                  // updates UI by showing message in list of items
                  setState(() {
                    widget.chatroom.messages.add(message);
                  });

                  // updates data in database and updates master chatroom list
                  Provider.of<model.ChatroomModel>(context, listen: false)
                      .addMessageToChatroom(context, widget.chatroom, message);

                  _scrollController.jumpTo(
                    _scrollController.position.maxScrollExtent +
                        MediaQuery.of(context).size.height ~/ 13,
                  );

//                  todo: add plugin for detecting connectivity

                  FocusScope.of(context).unfocus();
                  _controller.text = '';
                } else {
                  _scaffoldKey.currentState.showSnackBar(
                    SnackBar(
                      content: Text(
                        'Message must have content.',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                      action: SnackBarAction(
                        label: 'Dismiss'.toUpperCase(),
                        onPressed: () =>
                            _scaffoldKey.currentState.removeCurrentSnackBar(),
                      ),
                      behavior: SnackBarBehavior.floating,
                      backgroundColor: Theme.of(context).primaryColor,
                    ),
                  );
                }
              }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          widget.user.name,
          style: TextStyle(
            fontSize: 28,
            fontWeight: FontWeight.bold,
          ),
        ),
        elevation: 10,
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              controller: _scrollController,
              children: widget.chatroom.messages.map((message) {
                bool isMe =
                    message.sender == Provider.of<UserData>(context).userId;

                int timeDifference = (Timestamp.now().toDate())
                    .difference((message.timestamp.toDate()))
                    .inHours;

                bool isSameDay = timeDifference <= 23;

                return ChatListItem(
                  isMe: isMe,
                  isSameDay: isSameDay,
                  message: message,
                );
              }).toList(),
            ),
          ),
          buildMessageComposer(context),
        ],
      ),
    );
  }
}

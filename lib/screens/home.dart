import 'package:flutter/material.dart';

import 'package:isturya/providers/user_data.dart';
import 'package:isturya/screens/entry_screens/game_screen.dart';
import 'package:isturya/screens/entry_screens/messages_screen.dart';
import 'package:isturya/screens/entry_screens/search_screen.dart';

import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  static final id = 'home_screen';

  @override
  Widget build(BuildContext context) {
    final userId = Provider.of<UserData>(context).userId;

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              'Isturya'.toUpperCase(),
              style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.bold,
              ),
            ),
            elevation: 0.0,
            bottom: TabBar(
              isScrollable: true,
              labelColor: Colors.white,
              labelStyle: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                letterSpacing: 1.2,
              ),
              unselectedLabelColor: Colors.white60,
              indicator: BoxDecoration(
                  border: Border.all(width: 0, style: BorderStyle.none)),
              indicatorSize: TabBarIndicatorSize.label,
              labelPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              tabs: <Widget>[
                Text('Messages'),
                Text('Search'),
                Text('Games'),
              ],
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              MessagesScreen(),
              SearchScreen(currentUserId: userId),
              GameScreen(),
            ],
          ),
        ),
      ),
    );
  }
}

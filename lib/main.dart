import 'package:flutter/material.dart';

import 'package:isturya/providers/chatroom_model.dart';
import 'package:isturya/providers/user_data.dart';
import 'package:isturya/screens/authentication/login.dart';
import 'package:isturya/screens/authentication/signup.dart';
import 'package:isturya/screens/splash.dart';
import 'package:isturya/screens/home.dart';

import 'package:provider/provider.dart';

void main() => runApp(MyApp());

// todo: update dark theme from sml
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => UserData(), lazy: true),
        ChangeNotifierProvider(create: (context) => ChatroomModel()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Isturya',
        theme: ThemeData(
            primaryColor: Colors.blue[500],
            accentColor: Color(0xFFFEF9EB),
        ),
        home: SplashScreen(),
        routes: {
          HomeScreen.id: (context) => HomeScreen(),
          LoginScreen.id: (context) => LoginScreen(),
          SignupScreen.id: (context) => SignupScreen(),
        },
      ),
    );
  }
}

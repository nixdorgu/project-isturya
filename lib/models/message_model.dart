import 'package:cloud_firestore/cloud_firestore.dart';

class Message {
  final String sender; // uid of sender
  final String recipient; // uid of recepient
  final Timestamp timestamp; // timestamp and use datetimeformat
  final String content;

  // array union when adding messages
  Message({
    this.sender,
    this.recipient,
    this.timestamp,
    this.content,
  });

  // for searching for word in messages
  // checking who last sent a message
  // message tile purposes ^

  factory Message.fromDoc(DocumentSnapshot doc) {
    return Message(
      sender: doc['sender'],
      recipient: doc['recipient'],
      timestamp: doc['timestamp'],
      content: doc['content'],
    );
  }

  //
  factory Message.fromMap(Map<String, dynamic> map) {
    return Message(
        recipient: map['recipient'],
        content: map['content'],
        sender: map['sender'],
        timestamp: map['timestamp'],
    );
  }

  toMap() {
    return {
      'recipient': this.recipient,
      'content': this.content,
      'sender': this.sender,
      'timestamp': this.timestamp,
    };
  }
}
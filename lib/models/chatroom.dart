import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:isturya/models/message_model.dart';

class Chatroom {
  final String id;
  final List<String> users;
  List<Message> messages = [];

  Chatroom({this.id, this.users, var messages}) {
    if (messages.runtimeType == 'List<Message>') {
      this.messages = messages;
    } else {
      // to address DocumentSnapshot error
      messages.forEach((message) {
        this.messages.add(Message.fromMap(message));
      });
    }
  }

  factory Chatroom.fromDoc(DocumentSnapshot doc) {
    return Chatroom(
      id: doc.documentID,
      users: <String>[...doc['users']],
      messages: doc['messages'],
    );
  }
}

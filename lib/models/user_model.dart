import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  final id;
  final String name;
  final String profileURL;
  final Map chatrooms;

  User({
    this.id,
    this.name,
    this.profileURL,
    this.chatrooms
  });

  factory User.fromDoc(DocumentSnapshot doc) {
    return User(
      id: doc.documentID, // doc['id']
      name: doc['name'],
      profileURL: doc['profileURL'],
      chatrooms: doc['chatrooms']
    );
  }
}
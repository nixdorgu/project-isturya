import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:isturya/models/chatroom.dart';
import 'package:isturya/models/message_model.dart';
import 'package:isturya/services/database.dart';

enum State {loaded, loading}

class ChatroomModel extends ChangeNotifier {
  List<Chatroom> _chatrooms = [];
  State state = State.loading;
  String currentUserId;


  UnmodifiableListView<Chatroom> get allChatrooms {
    getChatrooms();
    return UnmodifiableListView(_chatrooms);
  }

  void toggleState() {
    if (state == State.loading) {
      this.state = State.loaded;
    }
  }

  void getChatrooms() async {
    var chatrooms = await DatabaseService.getChatroomStream(currentUserId: currentUserId);
    _chatrooms = chatrooms;

    await toggleState();
    await notifyListeners();
  }

  void addMessageToChatroom(BuildContext context, Chatroom chatroom, Message message) async {
    // update chatroom in database
    DatabaseService.sendMessageToChatroom(
      context: context,
      chatroom: chatroom,
      message: message,
    );

    // update chatroom list
    await getChatrooms();

    // update UI for chatroom list
    await notifyListeners();
  }

  void deleteChatroom(Chatroom chatroom) async {
    // todo: revisit this and test self-chat deletion again.
    List users = chatroom.users.where((user) => user != currentUserId).toList();

    // delete chatroom from database
    DatabaseService.deleteChatroom(chatroomId: chatroom.id, currentUserId: currentUserId, otherUserId: chatroom.users[0]);

    // update chatroom list
    await getChatrooms();

    // update UI for chatroom list
    await notifyListeners();
  }
}